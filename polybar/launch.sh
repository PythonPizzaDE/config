#!/usr/bin/env bash

killall -q polybar

polybar example & disown
polybar second & disown

echo "Bar launched..."
