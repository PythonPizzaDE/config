# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:$HOME/bin/Postman:/usr/bin:/usr/games:/usr/local/bin:/home/paul/bin/discord/:$PATH
export DOTNET_CLI_TELEMETRY_OPTOUT=true
export BROWSER=/usr/bin/chromium

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="apple" # set by `omz`

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode pip)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

function aliases() {
    alias qemu="qemu-system-x86_64"
    alias pwr="ping 8.8.8.8"
    alias docker="sudo docker"
    alias q="exit"
    alias cls="clear"
    alias vim="nvim"
    alias v="nvim"
    alias p="python3.10"
    # alias vi="nvim"
    alias n="nvim ."
    alias nrc="nvim ~/.config/nvim/init.lua"
    alias nrcj="cd ~/.config/nvim/ && nvim ~/.config/nvim/init.lua"
    alias nlua="nvim ~/.config/nvim/init.lua"
    alias mountd="sudo mount /dev/sdb1 /mnt/media"
    alias jd="cd /mnt/media"
    alias tsi="tsc --init"
    alias ntsi="npm init -y && tsc --init"
    alias python="python3"
    alias nqt="nvim ~/.config/qtile/config.py"
    alias nkitty="nvim ~/.config/kitty/kitty.conf"
    alias nfish="nvim ~/.config/fish/config.fish"
    alias doom=".emacs.d/bin/doom"
    alias aptup="sudo apt update -y && sudo apt upgrade -y"
    alias aptin="sudo apt install"
    alias aptrm="sudo apt remove"
    alias aptsch="apt search"
    alias ls="exa -lah --icons"
    alias npicom="nvim ~/.config/picom/picom.conf"
    alias nzsh="nvim ~/.zshrc"

    alias addall="git add ."
    alias branch="git branch"
    alias checkout="git checkout"
    alias commit="git commit"
    alias status="git status"
    alias push="git push"
    alias pull="git pull"
    alias merge="git merge"

    alias tags="ctags ./*"

    alias shuffle="~/shuffle.py"

    alias jqt='cd ~/.config/qtile'
    alias ji3='cd ~/.config/i3'

    alias xonotic="sudo /usr/games/Xonotic/xonotic-linux-glx.sh"

    alias b="./build"
}

# function joke() {
#     echo "$(python -c 'import joke_generator; print(joke_generator.generate())')" | cowsay | lolcat
# }

function joke() {
    clear
    echo "$(python3 ~/german_joke.py)" | cowsay | lolcat
}


function pkg_count() {
    echo $(expr $(apt list --installed | wc -l) + $(dpkg --list | wc -l))
}

function usb() {
    echo '''         ______________________________________
        / What do you call a bee that lives in \
        \ America? A USB.                      /
         --------------------------------------
                \   ^__^
                 \  (oo)\_______
                    (__)\       )\/\
                        ||----w |
                        ||     ||
            ''' | lolcat
}

# kitty @ set-colors --all --configured ~/.config/kitty/themes/gruvbox.conf
# neofetch
# fortune | cowsay -f $(ls /usr/share/cowsay/cows|shuf -n 1) | lolcat


# function powerline_precmd() {
#     PS1="$(powerline-shell --shell zsh $?)"
# }

# function install_powerline_precmd() {
#   for s in "${precmd_functions[@]}"; do
#     if [ "$s" = "powerline_precmd" ]; then
#       return
#     fi
#   done
#   precmd_functions+=(powerline_precmd)
# }

# if [ "$TERM" != "linux" ]; then
#     install_powerline_precmd
# fi

# source promptless.sh
#

aliases
# joke
# PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%} $%b "
colorscript random
source /home/paul/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

PATH="/home/paul/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/paul/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/paul/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/paul/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/paul/perl5"; export PERL_MM_OPT;
