# My .Dotfiles

Here are my most important .dotfiles I think.

## NeoVim

### the biggest config I've ever wrote from scratch

![nvim screenshot](screenshots/nvim.png "Screenshot of my NeoVim configuration")

## SimpleTerminal

### the only peace of configured suckless software on my system

![st screenshot](screenshots/st.png "Screenshot of my configured SimpleTerminal/SucklessTerminal")
