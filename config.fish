function startup
    neofetch
end

function aliases
    alias cls "clear"
    alias vim "nvim"
    alias vi "nvim"
    alias n "nvim ."
    alias nrc "nvim ~/.config/nvim/init.vim"
    alias nlua "nvim ~/.config/nvim/init.lua"
    alias mountd "sudo mount /dev/sdb1 /mnt/media"
    alias jd "cd /mnt/media"
    alias tsi "tsc --init"
    alias ntsi "npm init -y && tsc --init"
    alias python "python3"
    alias nqt "nvim ~/.config/qtile/config.py"
    alias nkitty "nvim ~/.config/kitty/kitty.conf"
    alias nfish "nvim ~/.config/fish/config.fish"
end

if status is-interactive
    aliases
    startup

#    set -gx fish_prompt_pwd_dir_length 0
#    set -g theme_display_group no
#    set -g theme_display_hostname no
#    set -g theme_color_user aa55ff
#    set -g theme_display_rw no
end

function fish_prompt
    powerline-shell --shell bare $status
end
