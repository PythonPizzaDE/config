from libqtile import bar, layout, widget
from libqtile.config import Screen

# bg = '#27292b5c'
bg = '#070c0d5c'

fontsize=24
arrow_font_size=int(fontsize*1.24)

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(background=bg, fontsize=fontsize, margin=0),
                widget.GroupBox(background=bg, fontsize=fontsize),
                widget.Prompt(background=bg, fontsize=fontsize),
                widget.WindowName(background=bg, fontsize=fontsize, max_chars=40),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                    fontsize=fontsize,
                    background=bg 
                ),
                widget.Volume(background=bg),
                widget.TextBox("press &lt;m-r&gt; to spawn",background=bg,  foreground="#d75f5f"),
                widget.Systray(background=bg, ),
                widget.Clock(background=bg, format='%y-%m-%d %a %H:%M:%S'),
                widget.QuickExit(background=bg),
            ],
            int(fontsize*1.63),
            # border_width=[2, 0, 2, 0],  # draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # borders are magenta
            margin=12,
            background=bg
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(background=bg, fontsize=fontsize, margin=0),
                widget.GroupBox(background=bg, fontsize=fontsize),
                widget.Prompt(background=bg, fontsize=fontsize),
                widget.WindowName(background=bg, fontsize=fontsize, max_chars=40),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                    fontsize=fontsize,
                    background=bg 
                ),
                widget.Volume(background=bg),
                widget.TextBox("press &lt;m-r&gt; to spawn",background=bg,  foreground="#d75f5f"),
                widget.Systray(background=bg, ),
                widget.Clock(background=bg, format='%y-%m-%d %a %H:%M:%S'),
                widget.QuickExit(background=bg),
            ],
            int(fontsize*1.63),
            # border_width=[2, 0, 2, 0],  # draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # borders are magenta
            margin=12,
            background=bg
        ),
    )
]
