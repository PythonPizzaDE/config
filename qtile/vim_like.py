from libqtile import bar, layout, widget
from libqtile.config import Screen

bg_light = '#c75134'
bg_mid = '#27292b'
bg_dark = '#1d1f21'

fontsize=24
arrow_font_size=int(fontsize*1.24)

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(background=bg_dark, fontsize=fontsize, margin=0),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.GroupBox(background=bg_light, fontsize=fontsize),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.Prompt(background=bg_dark, fontsize=fontsize),
                widget.WindowName(background=bg_dark, fontsize=fontsize, max_chars=40),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                    fontsize=fontsize,
                    background=bg_dark 
                ),
                widget.Volume(background=bg_light),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.TextBox("press &lt;m-r&gt; to spawn",background=bg_dark,  foreground="#d75f5f"),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.Systray(background=bg_light, ),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.Clock(background=bg_dark, format='%y-%m-%d %a %i:%m %p'),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.QuickExit(background=bg_light),
            ],
            int(fontsize*1.63),
            # border_width=[2, 0, 2, 0],  # draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # borders are magenta
            # margin=3
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(background=bg_dark, fontsize=fontsize, margin=0),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.GroupBox(background=bg_light, fontsize=fontsize),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.Prompt(background=bg_dark, fontsize=fontsize),
                widget.WindowName(background=bg_dark, fontsize=fontsize, max_chars=40),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                    fontsize=fontsize,
                    background=bg_dark 
                ),
                widget.Volume(background=bg_light),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.TextBox("press &lt;m-r&gt; to spawn",background=bg_dark,  foreground="#d75f5f"),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.Systray(background=bg_light, ),
                widget.TextBox('', foreground=bg_light, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_dark),
                widget.Clock(background=bg_dark, format='%y-%m-%d %a %i:%m %p'),
                widget.TextBox('', foreground=bg_dark, padding=0, fontsize=arrow_font_size, background=bg_mid),
                widget.TextBox('', foreground=bg_mid, padding=0, fontsize=arrow_font_size, background=bg_light),
                widget.QuickExit(background=bg_light),
            ],
            int(fontsize*1.63),
            # border_width=[2, 0, 2, 0],  # draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # borders are magenta
            # margin=3
        ),
    )
]
